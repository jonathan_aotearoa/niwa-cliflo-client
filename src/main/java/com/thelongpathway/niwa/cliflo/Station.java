package com.thelongpathway.niwa.cliflo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

/**
 * Simple JavaBean representing a weather station
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Station {

    @XmlElement
    private int agent;
    @XmlElement
    private String network;
    @XmlElement
    private String startDate;
    @XmlElement
    private String endDate;
    @XmlElement
    private int percentComplete;
    @XmlElement
    private String name;
    @XmlElement
    private String latitude;
    @XmlElement
    private String longitude;
    @XmlElement
    private BigDecimal distance;

    public Station() {
    }

    public Station(final int agent,
                   final String network,
                   final String startDate,
                   final String endDate,
                   final int percentComplete,
                   final String name,
                   final String latitude,
                   final String longitude,
                   final BigDecimal distance) {
        this.agent = agent;
        this.network = network;
        this.startDate = startDate;
        this.endDate = endDate;
        this.percentComplete = percentComplete;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Station station = (Station) o;
        return agent == station.agent;
    }

    @Override
    public int hashCode() {
        return agent;
    }

    public int getAgent() {
        return agent;
    }

    public String getNetwork() {
        return network;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getPercentComplete() {
        return percentComplete;
    }

    public String getName() {
        return name;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public BigDecimal getDistance() {
        return distance;
    }
}