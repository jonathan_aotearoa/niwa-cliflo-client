package com.thelongpathway.niwa.cliflo;

import com.thelongpathway.niwa.cliflo.impl.CliFloClientImpl;

public class CliFloClientFactory {

    public static CliFloClient newCliFloClient() {
        return new CliFloClientImpl();
    }
}