package com.thelongpathway.niwa.cliflo;

import com.google.common.collect.ListMultimap;
import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;
import com.thelongpathway.niwa.cliflo.params.DataTypeParams;
import com.thelongpathway.niwa.cliflo.params.LoginParams;
import com.thelongpathway.niwa.cliflo.params.StationDataParams;

import java.util.Collection;
import java.util.List;

/**
 * CliFlo client.
 */
public interface CliFloClient {

    /**
     * Logs this client into the CliFlo website
     *
     * @param loginParams the login parameters
     * @throws HttpException if an error occurs
     */
    void login(LoginParams loginParams) throws HttpException;

    /**
     * @param dataTypeParams        the required data type each station must provide
     * @param coordinatesCollection the list of coordinates
     * @param radiusKms             the radius in kms
     * @return a {@code ListMultimap} containing lists of {@code Station}s keyed by {@code CoordinatesParams}
     * @throws HttpException            if an error occurs
     * @throws InvalidResponseException if the response cannot be parsed
     */
    public ListMultimap<CoordinatesParams, Station> getStations(final DataTypeParams dataTypeParams,
                                                                final Collection<CoordinatesParams> coordinatesCollection,
                                                                final int radiusKms) throws HttpException, InvalidResponseException;

    /**
     * @param stationDataParams the request parameters
     * @return a new {@link StationDataResponse}
     * @throws HttpException if an error occurs
     */
    StationDataResponse getStationData(StationDataParams stationDataParams) throws HttpException, InvalidResponseException;

    /**
     * Iterates over the specified {@code stationsDataParams}, submitting a request for each element, i.e. station data
     * parameters. The first response containing data is returned.
     *
     * @param stationsDataParams the list of parameters for weather stations for a specific location
     * @return the response for the first weather station to return a response containing data, or {@code null} if none
     * of the weather stations return any data
     * @throws HttpException            if an error occurs
     * @throws InvalidResponseException if an error occurs parsing the raw response text for a specific station
     */
    StationDataResponse getStationData(List<StationDataParams> stationsDataParams) throws HttpException, InvalidResponseException;
}