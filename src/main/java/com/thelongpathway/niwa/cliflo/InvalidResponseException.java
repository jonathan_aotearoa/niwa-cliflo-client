package com.thelongpathway.niwa.cliflo;

public class InvalidResponseException extends Exception {

    private final String responseText;

    public InvalidResponseException(final String message, final String responseText) {
        super(message + "\n\n" + responseText);
        this.responseText = responseText;
    }

    public InvalidResponseException(final String message, final Throwable cause, final String responseText) {
        super(message + "\n\n" + responseText, cause);
        this.responseText = responseText;
    }

    public String getResponseText() {
        return responseText;
    }
}