package com.thelongpathway.niwa.cliflo.impl;

import com.thelongpathway.niwa.cliflo.HttpException;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for requests
 */
abstract class Request {

    private static final Logger LOG = LogManager.getLogger(Request.class);

    private final CloseableHttpClient httpClient;
    private final HttpClientContext httpClientContext;
    private final URI uri;

    /**
     * Constructs a new instance
     *
     * @param httpClient        the {@link CloseableHttpClient} that will be used to execute this request
     * @param httpClientContext the {@link HttpClientContext} for this request
     * @param uri               the request URI
     */
    protected Request(final CloseableHttpClient httpClient,
                      final HttpClientContext httpClientContext,
                      final String uri) {
        checkNotNull(httpClient, "httpClient cannot be null");
        checkNotNull(httpClientContext, "httpClientContext cannot be null");
        checkNotNull(uri, "uri cannot be null");
        this.httpClient = httpClient;
        this.httpClientContext = httpClientContext;
        try {
            this.uri = new URI(uri);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(uri + " is not a valid URI", e);
        }
    }

    /**
     * Executes this request
     *
     * @param params the parameters as a list of {@link NameValuePair}s
     * @return a {@link CloseableHttpResponse}
     * @throws HttpException if an error occurs
     */
    protected CloseableHttpResponse execute(final List<NameValuePair> params) throws HttpException {
        checkNotNull(params, "params cannot be null");
        final HttpPost httpPost = new HttpPost(uri);
        final CloseableHttpResponse response;
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            response = httpClient.execute(httpPost, httpClientContext);
        } catch (IOException e) {
            throw new HttpException("Error submitting " + httpPost.getRequestLine(), e);
        }
        final StatusLine statusLine = response.getStatusLine();
        if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
            throw new HttpException("Error submitting " + httpPost.getRequestLine(), statusLine);
        }
        return response;
    }

    /**
     * Utility method for quietly closing a {@link CloseableHttpResponse}
     *
     * @param response the {@link CloseableHttpResponse} to close
     */
    protected static void closeResponseQuietly(final CloseableHttpResponse response) {
        if (response != null) {
            try {
                response.close();
            } catch (IOException e) {
                LOG.warn("Error closing response. Status line: " + response.getStatusLine(), e);
            }
        }
    }
}