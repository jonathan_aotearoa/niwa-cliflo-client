package com.thelongpathway.niwa.cliflo.impl;

import com.thelongpathway.niwa.cliflo.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a 'Gen Form1 Proc' request
 */
class GenForm1ProcRequest extends Request {

    private static final String URI = "http://cliflo.niwa.co.nz/pls/niwp/wgenf.genform1_proc";

    /**
     * Constructs a new instance
     *
     * @param httpClient        the {@link CloseableHttpClient}
     * @param httpClientContext the {@link HttpClientContext}
     */
    public GenForm1ProcRequest(final CloseableHttpClient httpClient, final HttpClientContext httpClientContext) {
        super(httpClient, httpClientContext, URI);
    }

    public String submit(final List<NameValuePair> params) throws HttpException {
        checkNotNull(params, "params cannot be null");
        final CloseableHttpResponse response = execute(params);
        try {
            return EntityUtils.toString(response.getEntity()).trim();
        } catch (IOException e) {
            throw new HttpException("Error retrieving CSV from response", e);
        } finally {
            closeResponseQuietly(response);
        }
    }
}