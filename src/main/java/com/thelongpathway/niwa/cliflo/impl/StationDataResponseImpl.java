package com.thelongpathway.niwa.cliflo.impl;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.thelongpathway.niwa.cliflo.InvalidResponseException;
import com.thelongpathway.niwa.cliflo.StationDataResponse;
import com.thelongpathway.util.CsvReader;
import com.thelongpathway.util.InvalidCsvException;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

class StationDataResponseImpl implements StationDataResponse {

    private static final Pattern NUMBER_OF_ROWS_REGEX = Pattern.compile("^Total number of rows output = (\\d+)$");

    private final int agent;
    private final LocalDate date;
    private final String responseText;
    private final List<String> lines;
    private final CsvReader csvReader;
    private final int totalNumberOfRowsOutput;
    private final Map<String, String> stationInformation;
    private final Map<String, String> weatherData;

    /**
     * Constructs a new instance
     *
     * @param agent
     * @param date
     * @param responseText
     * @throws InvalidResponseException
     */
    public StationDataResponseImpl(final int agent, final LocalDate date, final String responseText) throws InvalidResponseException {
        checkNotNull(date, "date cannot be null");
        checkNotNull(responseText, "responseText cannot be null");
        this.agent = agent;
        this.date = date;
        this.responseText = responseText;
        lines = Splitter.on('\n').trimResults().splitToList(responseText);
        validateFirstList();
        csvReader = new CsvReader();
        totalNumberOfRowsOutput = extractTotalNumberOfRowsOutput();
        if (totalNumberOfRowsOutput == 0) {
            stationInformation = ImmutableMap.of();
            weatherData = ImmutableMap.of();
        } else {
            stationInformation = extractStationInformation();
            weatherData = extractWeatherData();
        }
    }

    @Override
    public int getAgent() {
        return agent;
    }

    @Override
    public LocalDate getDate() {
        return date;
    }

    @Override
    public String getStationName() {
        return stationInformation.get("Name");
    }

    @Override
    public int getTotalNumberOfRowsOutput() {
        return totalNumberOfRowsOutput;
    }

    @Override
    public Map<String, String> getStationInformation() {
        return stationInformation;
    }

    @Override
    public Map<String, String> getWeatherData() {
        return weatherData;
    }

    @Override
    public boolean hasRows() {
        return totalNumberOfRowsOutput > 0;
    }

    private void validateFirstList() throws InvalidResponseException {
        if (lines.isEmpty()) {
            throw new InvalidResponseException("Response is empty", responseText);
        }
        final String expectedFirstLine = "Station information:";
        final String firstLine = lines.get(0);
        if (!expectedFirstLine.equals(firstLine)) {
            throw new InvalidResponseException("Unexpected first line of responseText", responseText);
        }
    }

    private int extractTotalNumberOfRowsOutput() throws InvalidResponseException {
        for (final String line : lines) {
            final Matcher matcher = NUMBER_OF_ROWS_REGEX.matcher(line);
            if (matcher.matches()) {
                return Integer.parseInt(matcher.group(1));
            }
        }
        throw new InvalidResponseException("Error extracting total number of rows output", responseText);
    }

    private Map<String, String> extractStationInformation() throws InvalidResponseException {
        final String csv = Joiner.on('\n').join(lines.subList(1, 3));
        try {
            return ImmutableMap.copyOf(csvReader.read(csv).get(0));
        } catch (InvalidCsvException e) {
            throw new InvalidResponseException("Error extracting station data", e, responseText);
        }
    }

    public Map<String, String> extractWeatherData() throws InvalidResponseException {
        final String csv = Joiner.on('\n').join(lines.subList(10, 12));
        try {
            return csvReader.read(csv).get(0);
        } catch (InvalidCsvException e) {
            throw new InvalidResponseException("Error extracting weather data", e, responseText);
        }
    }
}