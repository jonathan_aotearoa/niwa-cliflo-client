package com.thelongpathway.niwa.cliflo.impl;

import com.google.common.collect.ImmutableList;
import com.thelongpathway.niwa.cliflo.HttpException;
import com.thelongpathway.niwa.cliflo.params.LoginParams;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a CliFlo login request
 */
class LoginRequest extends Request {

    private static final String URI = "http://cliflo.niwa.co.nz/pls/niwp/wa.logindb";

    /**
     * Constructs a new instance
     *
     * @param httpClient        the {@link CloseableHttpClient} that will be used to execute this request
     * @param httpClientContext the {@link HttpClientContext} for this request
     */
    public LoginRequest(CloseableHttpClient httpClient, final HttpClientContext httpClientContext) {
        super(httpClient, httpClientContext, URI);
    }

    /**
     * Submits this request
     *
     * @param loginParams the login loginParams
     * @throws HttpException if an error occurs
     */
    public void submit(final LoginParams loginParams) throws HttpException {
        checkNotNull(loginParams, "loginParams cannot be null");
        final List<NameValuePair> params = buildRequestParameters(loginParams);
        final CloseableHttpResponse response = execute(params);
        closeResponseQuietly(response);
    }

    private static List<NameValuePair> buildRequestParameters(final LoginParams loginParams) {
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.addAll(loginParams.params);
        builder.add(new BasicNameValuePair("submit", "login"));
        builder.add(new BasicNameValuePair("ispopup", "false"));
        return builder.build();
    }
}