package com.thelongpathway.niwa.cliflo.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.thelongpathway.niwa.cliflo.InvalidResponseException;
import com.thelongpathway.niwa.cliflo.Station;
import com.thelongpathway.niwa.cliflo.StationsByLocationResponse;
import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

class StationsByLocationResponseImpl implements StationsByLocationResponse {

    private static final Pattern NUMBER_OF_STATIONS_REGEX = Pattern.compile("^Number of stations in list: (\\d+)$");
    private static final int N_TOKENS = 9;
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-uuuu");

    private final CoordinatesParams coordinatesParams;
    private final int radiusKms;
    private final String responseText;
    private final List<String> lines;
    private final int numberOfStations;
    private final List<Station> stations;

    /**
     * Constructs a new instance
     *
     * @param coordinatesParams the location for the associated request
     * @param radiusKms         the radius for the associated request
     * @param responseText      the CliFlo response text
     * @throws InvalidResponseException if an error occurs parsing the response text
     */
    public StationsByLocationResponseImpl(final CoordinatesParams coordinatesParams,
                                          final int radiusKms,
                                          final String responseText) throws InvalidResponseException {
        checkNotNull(responseText, "responseText cannot be null");
        this.coordinatesParams = coordinatesParams;
        this.radiusKms = radiusKms;
        this.responseText = responseText.trim();
        lines = splitIntoLines();
        validateFirstLine();
        numberOfStations = extractNumberOfStations();
        stations = extractStations();
    }

    @Override
    public CoordinatesParams getCoordinatesParams() {
        return coordinatesParams;
    }

    @Override
    public int getRadiusKms() {
        return radiusKms;
    }

    @Override
    public int getNumberOfStations() {
        return numberOfStations;
    }

    @Override
    public List<Station> getStations() {
        return stations;
    }

    private List<String> splitIntoLines() {
        return ImmutableList.copyOf(Splitter.on('\n').trimResults().splitToList(responseText));
    }

    private void validateFirstLine() throws InvalidResponseException {
        if (lines.isEmpty()) {
            throw new InvalidResponseException("Response is empty", responseText);
        }
        final String expectedFirstLine = "Station Listing";
        final String firstLine = lines.get(0);
        if (!expectedFirstLine.equals(firstLine)) {
            throw new InvalidResponseException("Unexpected first line of responseText", responseText);
        }
    }

    private int extractNumberOfStations() throws InvalidResponseException {
        for (final String line : lines) {
            final Matcher matcher = NUMBER_OF_STATIONS_REGEX.matcher(line);
            if (matcher.matches()) {
                return Integer.parseInt(matcher.group(1));
            }
        }
        throw new InvalidResponseException("Error extracting number of stations", responseText);
    }

    private List<Station> extractStations() throws InvalidResponseException {
        if (numberOfStations == 0) {
            return ImmutableList.of();
        }
        final List<String> rows = lines.subList(7, lines.size() - 1);
        final ImmutableList.Builder<Station> stations = new ImmutableList.Builder<>();
        for (final String row : rows) {
            stations.add(createStation(row));
        }
        return stations.build();
    }

    private Station createStation(final String csvRow) throws InvalidResponseException {
        final List<String> tokens = Splitter.on('\t').trimResults().splitToList(csvRow);
        if (tokens.size() != N_TOKENS) {
            final String template = "Expected %s tokens but found %s. CSV row: %s";
            final String errorMsg = String.format(template, N_TOKENS, tokens.size(), csvRow);
            throw new InvalidResponseException(errorMsg, responseText);
        }
        final int agent = Integer.parseInt(tokens.get(0));
        final String network = tokens.get(1);
        final String startDate = convertDateToIsoFormat(tokens.get(2));
        final String endDate = convertDateToIsoFormat(tokens.get(3));
        final int percentComplete = Integer.parseInt(tokens.get(4));
        final String name = tokens.get(5);
        final String latitude = tokens.get(6);
        final String longitude = tokens.get(7);
        final BigDecimal distance = new BigDecimal(tokens.get(8));
        return new Station(agent, network, startDate, endDate, percentComplete, name, latitude, longitude, distance);
    }

    private String convertDateToIsoFormat(final String date) {
        final LocalDate localDate = LocalDate.parse(date, DATE_TIME_FORMATTER);
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
}