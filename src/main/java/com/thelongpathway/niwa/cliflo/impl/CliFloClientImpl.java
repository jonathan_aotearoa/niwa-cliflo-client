package com.thelongpathway.niwa.cliflo.impl;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;
import com.thelongpathway.niwa.cliflo.*;
import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;
import com.thelongpathway.niwa.cliflo.params.DataTypeParams;
import com.thelongpathway.niwa.cliflo.params.LoginParams;
import com.thelongpathway.niwa.cliflo.params.StationDataParams;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * {@link CliFloClient} implementation
 */
public class CliFloClientImpl implements CliFloClient {

    private static final Logger LOG = LogManager.getLogger(CliFloClientImpl.class);

    private final HttpClientContext httpClientContext;
    private final CloseableHttpClient httpClient;

    /**
     * Creates a new instance.
     */
    public CliFloClientImpl() {
        final BasicCookieStore cookieStore = new BasicCookieStore();
        httpClientContext = HttpClientContext.create();
        httpClientContext.setCookieStore(cookieStore);
        httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
    }

    @Override
    public void login(final LoginParams loginParams) throws HttpException {
        final LoginRequest loginRequest = new LoginRequest(httpClient, httpClientContext);
        loginRequest.submit(loginParams);
    }

    @Override
    public ListMultimap<CoordinatesParams, Station> getStations(final DataTypeParams dataTypeParams,
                                                                final Collection<CoordinatesParams> coordinatesCollection,
                                                                final int radiusKms)
            throws HttpException, InvalidResponseException {
        // Set the data set
        final GenForm1ProcRequest dataSetRequest = new GenForm1ProcRequest(httpClient, httpClientContext);
        dataSetRequest.submit(dataTypeParams.params);
        // Get stations by location
        final StationsByLocationRequest stationsByLocationRequest = new StationsByLocationRequest(httpClient, httpClientContext);
        final ImmutableListMultimap.Builder<CoordinatesParams, Station> builder = ImmutableListMultimap.builder();
        for (final CoordinatesParams coordinates : coordinatesCollection) {
            final StationsByLocationResponse response = stationsByLocationRequest.submit(coordinates, radiusKms);
            final List<Station> stations = response.getStations();
            builder.putAll(coordinates, stations);
        }
        return builder.build();
    }

    @Override
    public StationDataResponse getStationData(final StationDataParams stationDataParams)
            throws HttpException, InvalidResponseException {
        checkNotNull(stationDataParams, "stationDataParams cannot be null");
        LOG.debug("Requesting data for agent ", stationDataParams.agent);
        final GenForm1ProcRequest stationDataRequest = new GenForm1ProcRequest(httpClient, httpClientContext);
        final String responseText = stationDataRequest.submit(stationDataParams.params);
        return new StationDataResponseImpl(stationDataParams.agent, stationDataParams.date, responseText);
    }

    @Override
    public StationDataResponse getStationData(final List<StationDataParams> stationsDataParams)
            throws HttpException, InvalidResponseException {
        checkNotNull(stationsDataParams, "stationsDataParams cannot be null");
        for (final StationDataParams stationDataParams : stationsDataParams) {
            final StationDataResponse response = getStationData(stationDataParams);
            if (response.hasRows()) {
                return response;
            }
        }
        LOG.info("None of the stations returned any rows. Returning null");
        return null;
    }
}