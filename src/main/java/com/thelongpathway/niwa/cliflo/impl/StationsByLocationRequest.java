package com.thelongpathway.niwa.cliflo.impl;

import com.google.common.collect.ImmutableList;
import com.thelongpathway.niwa.cliflo.HttpException;
import com.thelongpathway.niwa.cliflo.InvalidResponseException;
import com.thelongpathway.niwa.cliflo.StationsByLocationResponse;
import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;
import com.thelongpathway.niwa.cliflo.params.MimeSelectionParam;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents a 'stations by location' request.
 * A stations by location request retrieves all the weather stations within the specified radius of the specified
 * coordinates.
 */
class StationsByLocationRequest extends Request {

    private static final String URI = "http://cliflo.niwa.co.nz/pls/niwp/wstn.get_stn";

    /**
     * Constructs a new instance
     *
     * @param httpClient        the {@link CloseableHttpClient} that will be used to execute this request
     * @param httpClientContext the {@link HttpClientContext} for this request
     */
    public StationsByLocationRequest(final CloseableHttpClient httpClient, final HttpClientContext httpClientContext) {
        super(httpClient, httpClientContext, URI);
    }

    /**
     * Submits this request
     *
     * @param coordinatesParams the coordinatesParams
     * @param radiusKms         the radius in Kms
     * @return a new {@link StationsByLocationResponseImpl}
     * @throws HttpException            if an error occurs
     * @throws InvalidResponseException if the an error occurs parsing the raw response text
     */
    public StationsByLocationResponse submit(final CoordinatesParams coordinatesParams, final int radiusKms) throws HttpException, InvalidResponseException {
        checkNotNull(coordinatesParams, "coordinatesParams cannot be null");
        checkArgument(radiusKms > 0, "radiusKms must be greater than zero");
        final List<NameValuePair> params = buildStationParams(coordinatesParams, radiusKms);
        final CloseableHttpResponse response = execute(params);
        try {
            // the raw CSV containing all the weather stations within specified radius of the specified location
            final String responseText = EntityUtils.toString(response.getEntity());
            return new StationsByLocationResponseImpl(coordinatesParams, radiusKms, responseText);
        } catch (IOException e) {
            throw new HttpException("Error retrieving CSV from response", e);
        } finally {
            closeResponseQuietly(response);
        }
    }

    private static List<NameValuePair> buildStationParams(final CoordinatesParams coordinatesParams, final int radiusKms) {
        final ImmutableList.Builder<NameValuePair> params = new ImmutableList.Builder<>();
        params.add(new BasicNameValuePair("ccomb_dt", "or"));
        params.add(new BasicNameValuePair("cstnstr", ""));
        params.add(new BasicNameValuePair("cNet", ""));
        params.add(new BasicNameValuePair("cAgent", ""));
        params.add(new BasicNameValuePair("cRegion", ""));
        params.add(new BasicNameValuePair("cstype", "latlongc"));
        params.addAll(coordinatesParams.params);
        params.add(new BasicNameValuePair("crad", String.valueOf(radiusKms)));
        params.add(new BasicNameValuePair("status", "open"));
        params.add(MimeSelectionParam.TAB_PLAIN.param);
        params.add(new BasicNameValuePair("Submit", "Get+Station+List"));
        return params.build();
    }
}