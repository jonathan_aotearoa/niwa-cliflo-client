package com.thelongpathway.niwa.cliflo;

import java.time.LocalDate;
import java.util.Map;

/**
 * Defines the contract for the response to a weather station data request
 */
public interface StationDataResponse {

    /**
     * @return the unique weather station identifier
     */
    int getAgent();

    /**
     * @return the date of the data
     */
    LocalDate getDate();

    /**
     * @return the weather station name
     */
    String getStationName();

    int getTotalNumberOfRowsOutput();

    /**
     * @return {@code true} if the raw response contained any rows, {@code false} otherwise
     */
    boolean hasRows();

    Map<String, String> getStationInformation();

    Map<String, String> getWeatherData();
}