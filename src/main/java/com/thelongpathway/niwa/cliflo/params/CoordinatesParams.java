package com.thelongpathway.niwa.cliflo.params;

import com.google.common.collect.ImmutableList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable class representing the parameters that define location coordinates.
 */
public class CoordinatesParams {

    private static final Pattern REGEX = Pattern.compile("^-?\\d+\\.\\d+$");
    private static final String LATITUDE_PARAM = "clat1";
    private static final String LONGITUDE_PARAM = "clong1";

    public final List<NameValuePair> params;
    private final String latitude;
    private final String longitude;

    /**
     * Create a new instance
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    public CoordinatesParams(final String latitude, final String longitude) {
        validate(latitude, longitude);
        this.latitude = latitude;
        this.longitude = longitude;
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.add(new BasicNameValuePair(LATITUDE_PARAM, latitude));
        builder.add(new BasicNameValuePair(LONGITUDE_PARAM, longitude));
        this.params = builder.build();
    }

    @Override
    public String toString() {
        return latitude + "," + longitude;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CoordinatesParams that = (CoordinatesParams) o;
        return latitude.equals(that.latitude) && longitude.equals(that.longitude);
    }

    @Override
    public int hashCode() {
        int result = latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        return result;
    }

    private static void validate(final String latitude, final String longitude) {
        checkNotNull(latitude, "latitude cannot be null");
        checkNotNull(longitude, "longitude cannot be null");
        if (!REGEX.matcher(latitude).matches()) {
            throw new IllegalArgumentException("Invalid latitude: " + latitude);
        }
        if (!REGEX.matcher(longitude).matches()) {
            throw new IllegalArgumentException("Invalid longitude: " + longitude);
        }
    }
}