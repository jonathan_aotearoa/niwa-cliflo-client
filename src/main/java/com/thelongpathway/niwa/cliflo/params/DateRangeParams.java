package com.thelongpathway.niwa.cliflo.params;

import com.google.common.collect.ImmutableList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable class representing the parameters that define the date range for a single day.
 */
public class DateRangeParams {

    public final List<NameValuePair> params;

    /**
     * Creates a new instance
     *
     * @param localDate the {@link LocalDate} for the day that defines this date range
     */
    public DateRangeParams(final LocalDate localDate) {
        checkNotNull(localDate, "localDate cannot be null");
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.add(new BasicNameValuePair("dateauswahl", "wgenf.genform1?fset=defdate"));
        builder.addAll(buildDateParams(localDate, 1));
        builder.addAll(buildDateParams(localDate, 2));
        params = builder.build();
    }

    private static List<NameValuePair> buildDateParams(final LocalDate localDate, final int number) {
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.add(new BasicNameValuePair(String.format("date%s_1", number), String.valueOf(localDate.getYear())));
        builder.add(new BasicNameValuePair(String.format("date%s_2", number), String.valueOf(localDate.getMonthValue())));
        builder.add(new BasicNameValuePair(String.format("date%s_3", number), String.valueOf(localDate.getDayOfMonth())));
        builder.add(new BasicNameValuePair(String.format("date%s_4", number), "00"));
        return builder.build();
    }
}