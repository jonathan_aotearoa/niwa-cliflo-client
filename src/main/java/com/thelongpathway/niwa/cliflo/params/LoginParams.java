package com.thelongpathway.niwa.cliflo.params;

import com.google.common.collect.ImmutableList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An immutable class representing the parameters that define login credentials.
 */
public class LoginParams {

    private static final String USERNAME_PARAM = "cusername";
    private static final String PASSWORD_PARAM = "cpwd";

    public List<NameValuePair> params;

    /**
     * Creates a new instance
     *
     * @param username the username
     * @param password the password
     */
    public LoginParams(final String username, final String password) {
        checkNotNull(username, "username cannot be null");
        checkNotNull(password, "password cannot be null");
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.add(new BasicNameValuePair(USERNAME_PARAM, username));
        builder.add(new BasicNameValuePair(PASSWORD_PARAM, password));
        params = builder.build();
    }
}