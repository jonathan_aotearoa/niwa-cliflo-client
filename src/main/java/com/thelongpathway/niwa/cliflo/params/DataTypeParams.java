package com.thelongpathway.niwa.cliflo.params;

import com.google.common.collect.ImmutableList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for data type parameters
 */
public abstract class DataTypeParams {

    public final List<NameValuePair> params;

    /**
     * @param dataType the name of this data type
     * @param params
     */
    protected DataTypeParams(final String dataType, final NameValuePair... params) {
        checkNotNull(dataType, "dataType cannot be null");
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.add(new BasicNameValuePair("cselect", "wgenf.genform1?fset=defdtype"));
        builder.addAll(Arrays.asList(params));
        builder.add(new BasicNameValuePair("dt1", dataType));
        this.params = builder.build();
    }
}