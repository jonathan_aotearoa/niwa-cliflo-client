package com.thelongpathway.niwa.cliflo.params;

import com.google.common.collect.ImmutableList;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents the immutable list of parameters that constitute a request for weather station data
 */
public class StationDataParams {

    /**
     * The agent, i.e. the weather station's unique identifier
     */
    public final int agent;

    /**
     * The station data type
     */
    public final DataTypeParams dataTypeParams;

    /**
     * The date we want weather station data for
     */
    public final LocalDate date;

    /**
     * The parameter name-value pairs the constitute the request
     */
    public final List<NameValuePair> params;

    /**
     * Constructs a new instance
     *
     * @param agent          the agent, i.e. the unique id of the weather station's we're requesting data for
     * @param dataTypeParams the data type we're requesting
     * @param date           the date we're requesting data for
     */
    public StationDataParams(final int agent, final DataTypeParams dataTypeParams, final LocalDate date) {
        checkNotNull(dataTypeParams, "dataTypeParams cannot be null");
        checkNotNull(date, "date cannot be null");
        this.agent = agent;
        this.dataTypeParams = dataTypeParams;
        this.date = date;
        final ImmutableList.Builder<NameValuePair> builder = new ImmutableList.Builder<>();
        builder.addAll(dataTypeParams.params);
        builder.add(new BasicNameValuePair("auswahl", "wgenf.genform1?fset=defagent"));
        builder.add(new BasicNameValuePair("agents", String.valueOf(agent)));
        builder.addAll(new DateRangeParams(date).params);
        builder.add(new BasicNameValuePair("formatselection", "wgenf.genform1?fset=deffmt"));
        builder.add(new BasicNameValuePair("TSselection", "NZST"));
        builder.add(new BasicNameValuePair("dateformat", "0"));
        builder.add(new BasicNameValuePair("Splitdate", "N"));
        builder.add(MimeSelectionParam.TAB_PLAIN.param);
        builder.add(new BasicNameValuePair("cstn_id", "A"));
        builder.add(new BasicNameValuePair("cdata_order", "DS"));
        builder.add(new BasicNameValuePair("submit_sq", "Send Query"));
        params = builder.build();
    }
}