package com.thelongpathway.niwa.cliflo.params;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * An enumeration representing 'mime selection' parameter values
 */
public enum MimeSelectionParam {

    TAB_PLAIN("tabplain");

    private static final String PARAM_NAME = "mimeselection";

    public final NameValuePair param;

    MimeSelectionParam(final String paramValue) {
        param = new BasicNameValuePair(PARAM_NAME, paramValue);
    }
}