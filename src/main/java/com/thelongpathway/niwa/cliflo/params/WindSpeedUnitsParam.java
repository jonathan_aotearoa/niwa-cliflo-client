package com.thelongpathway.niwa.cliflo.params;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * An enumeration representing 'wind speed units' parameter values
 */
public enum WindSpeedUnitsParam {

    METERS_PER_SECOND("ms"), KM_PER_HOUR("kmhr"), KNOTS("knots");

    public static final String PARAM_NAME = "prm2";

    public final NameValuePair param;

    WindSpeedUnitsParam(final String paramValue) {
        param = new BasicNameValuePair(PARAM_NAME, paramValue);
    }
}