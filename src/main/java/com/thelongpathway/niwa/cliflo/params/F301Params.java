package com.thelongpathway.niwa.cliflo.params;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * Represents a 'Form 301' data type, which equates to 'combined daily data  at 9am (local)'.
 * see@ <a hre="http://cliflo-niwa.niwa.co.nz/pls/niwp/wh.do_help?id=ls_f301">http://cliflo-niwa.niwa.co.nz/pls/niwp/wh.do_help?id=ls_f301</a> for details
 */
public class F301Params extends DataTypeParams {

    public enum Option {

        BASIC("311"), EXTENDED("312"), FULL("313");

        private static final String PARAM_NAME = "prm1";
        private final NameValuePair param;

        Option(final String paramValue) {
            param = new BasicNameValuePair(PARAM_NAME, paramValue);
        }
    }

    /**
     * Constructs a new instance with the default option and windspeed units of {@link Option#EXTENDED} and
     * {@link WindSpeedUnitsParam#METERS_PER_SECOND} respectively
     */
    public F301Params() {
        this(Option.EXTENDED, WindSpeedUnitsParam.METERS_PER_SECOND);
    }

    /**
     * Constructs a new instance
     *
     * @param option              the F301 option
     * @param windSpeedUnitsParam the wind speed units
     */
    public F301Params(final Option option, final WindSpeedUnitsParam windSpeedUnitsParam) {
        super("ls_f301,1,2", option.param, windSpeedUnitsParam.param);
    }
}