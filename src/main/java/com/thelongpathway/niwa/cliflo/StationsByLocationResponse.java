package com.thelongpathway.niwa.cliflo;

import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;

import java.util.List;

/**
 * Represents the response for a 'stations by location' request
 */
public interface StationsByLocationResponse {

    /**
     * @return the location that was specified for the associated request
     */
    CoordinatesParams getCoordinatesParams();

    /**
     * @return the radius that was specified for the associated request
     */
    int getRadiusKms();

    /**
     * @return the number of stations contained in this response
     */
    int getNumberOfStations();

    /**
     * @return the list of stations contained in this response
     */
    List<Station> getStations();
}