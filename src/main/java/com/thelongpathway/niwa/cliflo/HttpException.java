package com.thelongpathway.niwa.cliflo;

import org.apache.http.StatusLine;

public class HttpException extends Exception {

    public HttpException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public HttpException(final String message, final StatusLine statusLine) {
        super(message + ": " + statusLine);
    }
}