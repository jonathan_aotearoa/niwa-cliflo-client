package com.thelongpathway.util;

public class InvalidCsvException extends Exception {

    private final String csv;

    public InvalidCsvException(final String message, final String csv) {
        super(message);
        this.csv = csv;
    }

    public String getCsv() {
        return csv;
    }
}