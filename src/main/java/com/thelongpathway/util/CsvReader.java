package com.thelongpathway.util;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static com.google.common.base.Preconditions.checkNotNull;

public class CsvReader {

    private static final Splitter LINE_SPLITTER = Splitter.on('\n');

    private final Splitter splitter;

    /**
     * Constructs a new instance using '\t' as the delimiter character
     */
    public CsvReader() {
        this('\t');
    }

    /**
     * Constructs a new instance
     *
     * @param delimiter the delimiter character
     */
    public CsvReader(final char delimiter) {
        splitter = Splitter.on(delimiter);
    }

    /**
     * @param csv the CSV text
     * @return
     * @throws InvalidCsvException if an error occurs reading the CSV text
     */
    public List<Map<String, String>> read(final String csv) throws InvalidCsvException {
        checkNotNull(csv, "csv cannot be null");
        final List<String> lines = LINE_SPLITTER.splitToList(csv);
        if (lines.size() < 2) {
            throw new InvalidCsvException("csv must contain at least two lines", csv);
        }
        final List<String> headings = splitter.splitToList(lines.get(0));
        final ImmutableList.Builder<Map<String, String>> listBuilder = new ImmutableList.Builder<>();
        for (int lineNumber = 1; lineNumber < lines.size(); lineNumber++) {
            final String line = lines.get(lineNumber);
            final List<String> values = splitter.splitToList(line);
            if (headings.size() != values.size()) {
                final String template = "Expected %s values but found %s. Line number %s";
                final String errorMsg = String.format(template, headings.size(), values.size(), lineNumber + 1);
                throw new InvalidCsvException(errorMsg, csv);
            }
            final ImmutableMap.Builder<String, String> mapBuilder = new ImmutableMap.Builder<>();
            IntStream.range(0, values.size()).forEach(columnNumber -> mapBuilder.put(headings.get(columnNumber), values.get(columnNumber)));
            listBuilder.add(mapBuilder.build());
        }
        return listBuilder.build();
    }
}