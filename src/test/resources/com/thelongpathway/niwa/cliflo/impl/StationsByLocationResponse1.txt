Station Listing
Selected DataTypes were combined by: "ANY selected datatypes MAY exist (OR'd)"
Selected Datatypes are:
Code	Description
312	F301_Data: Climate Obs From Stns With Extended Instrumentation

Agent	Network	Start_Date	End_Date	Percent_Complete	Name	Lat(dec_deg)	Long(dec_deg)	Dist_Km
1002	A42462	01-Jun-1999	11-Jun-2015	90	Cape Reinga Aws	-34.432	172.682	    0.7
18183	A53026	01-Jul-2000	13-Jun-2015	100	Kaitaia Aero Ews	-35.0677	173.2874	   90.4
1041	A53125	01-May-1985	12-Jun-2015	100	Kaitaia Observatory	-35.13352	173.26294	   95.0
17067	A53127	01-Jan-1999	13-Jun-2015	100	Kaitaia Ews	-35.135	173.262	   95.1
Number of stations in list: 4