package com.thelongpathway.niwa.cliflo.impl;

import com.thelongpathway.niwa.cliflo.InvalidResponseException;
import com.thelongpathway.niwa.cliflo.ResourceUtils;
import com.thelongpathway.niwa.cliflo.StationDataResponse;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class StationDataResponseImplTest {

    private String responseText;
    private StationDataResponse response;

    @Test
    public void responseWithData() throws InvalidResponseException {
        given_a_response_with_rows();
        when_a_response_created();
        then_the_station_name_is("Cape Reinga Aws");
        then_the_total_number_of_row_output_is(5);
        then_there_are_n_weather_data_items(23);
    }

    @Test
    public void responseWithNoRows() throws InvalidResponseException {
        given_a_response_with_no_rows();
        when_a_response_created();
        then_the_station_name_is(null);
        then_the_response_has_no_rows();
        then_the_total_number_of_row_output_is(0);
        then_the_weather_data_is_empty();
    }

    private void given_a_response_with_rows() {
        responseText = ResourceUtils.getResourceAsString(getClass().getPackage(), "StationDataResponseWithRows.txt");
    }

    private void given_a_response_with_no_rows() {
        responseText = ResourceUtils.getResourceAsString(getClass().getPackage(), "StationDataResponseWithNoRows.txt");
    }

    private void when_a_response_created() throws InvalidResponseException {
        response = new StationDataResponseImpl(1, LocalDate.now(), responseText);
    }

    private void then_the_station_name_is(final String name) throws InvalidResponseException {
        assertEquals(name, response.getStationName());
    }

    private void then_the_response_has_no_rows() {
        assertFalse(response.hasRows());
    }

    private void then_the_total_number_of_row_output_is(final int totalNumberOfRowsOutput) {
        assertEquals(totalNumberOfRowsOutput, response.getTotalNumberOfRowsOutput());
    }

    private void then_there_are_n_weather_data_items(final int nDataItems) throws InvalidResponseException {
        assertEquals(nDataItems, response.getWeatherData().size());
    }

    private void then_the_weather_data_is_empty() throws InvalidResponseException {
        assertTrue(response.getWeatherData().isEmpty());
    }
}