package com.thelongpathway.niwa.cliflo.impl;

import com.thelongpathway.niwa.cliflo.InvalidResponseException;
import com.thelongpathway.niwa.cliflo.ResourceUtils;
import com.thelongpathway.niwa.cliflo.StationsByLocationResponse;
import com.thelongpathway.niwa.cliflo.params.CoordinatesParams;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.assertEquals;

public class StationsByLocationResponseImplTest {

    private String responseText;

    @Before
    public void before() throws IOException {
        final File file = ResourceUtils.getResourceAsFile(getClass().getPackage(), "StationsByLocationResponse2.txt");
        responseText = new String(Files.readAllBytes(file.toPath()));
    }

    @Test
    public void init() throws InvalidResponseException {
        final CoordinatesParams coordinatesParams = new CoordinatesParams("-45.0", "180.0");
        final StationsByLocationResponse response = new StationsByLocationResponseImpl(coordinatesParams, 100, responseText);
        assertEquals(30, response.getNumberOfStations());
        assertEquals(30, response.getStations().size());
    }
}