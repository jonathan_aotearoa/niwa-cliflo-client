package com.thelongpathway.niwa.cliflo.params;

import org.junit.Test;

public class CoordinatesParamsTest {

    @Test
    public void valid() {
        new CoordinatesParams("-45.01", "180.789");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidLatitude() {
        new CoordinatesParams("-45,0", "180.0");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidLongitude() {
        new CoordinatesParams("-45.0", "180.0b");
    }
}