package com.thelongpathway.niwa.cliflo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ResourceUtils {

    public static File getResourceAsFile(final String resourceName) {
        checkNotNull(resourceName, "resourceName cannot be null");
        final URL resourceUrl = ResourceUtils.class.getClassLoader().getResource(resourceName);
        assertNotNull(resourceUrl);
        final File file = new File(resourceUrl.getFile());
        assertTrue(file.isFile());
        return file;
    }

    public static File getResourceAsFile(final Package pkg, final String fileName) {
        checkNotNull(pkg, "pkg cannot be null");
        checkNotNull(fileName, "fileName cannot be null");
        final String resourceName = toPath(pkg) + fileName;
        return getResourceAsFile(resourceName);
    }

    public static String getResourceAsString(final String resource) {
        final File file = getResourceAsFile(resource);
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getResourceAsString(final Package pkg, final String fileName) {
        checkNotNull(pkg, "pkg cannot be null");
        checkNotNull(fileName, "fileName cannot be null");
        final String resourceName = toPath(pkg) + fileName;
        return getResourceAsString(resourceName);
    }

    private static String toPath(final Package pkg) {
        if (pkg.getName().isEmpty()) {
            return "";
        }
        return pkg.getName().replaceAll("\\.", "/") + "/";
    }
}