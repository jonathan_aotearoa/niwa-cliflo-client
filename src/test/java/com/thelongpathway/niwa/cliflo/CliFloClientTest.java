package com.thelongpathway.niwa.cliflo;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.thelongpathway.niwa.cliflo.params.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;

public class CliFloClientTest {

    private CliFloClient client;
    private DataTypeParams dataTypeParams;
    private LocalDate localDate;
    private CoordinatesParams coordinatesParams;
    private ListMultimap<CoordinatesParams, Station> stationsByLocation;
    private StationDataParams stationDataParams;
    private StationDataResponse stationDataResponse;

    @Before
    public void before() throws HttpException {
        client = CliFloClientFactory.newCliFloClient();
        dataTypeParams = new F301Params();
        localDate = LocalDate.parse("2013-10-16", DateTimeFormatter.ISO_LOCAL_DATE);
    }

    @Test
    public void noWeatherStationsWithinOneKmOfRuapehu() throws InvalidResponseException, HttpException {
        given_the_client_is_logged_in();
        given_coordinate_params_for_mt_ruapehu();
        when_i_request_a_list_of_weather_stations_within_n_kms(1);
        then_the_number_of_returned_stations_is(0);
    }

    @Test
    public void sixWeatherStationsWithinFiftyKmsOfRuapehu() throws HttpException, InvalidResponseException {
        given_the_client_is_logged_in();
        given_coordinate_params_for_mt_ruapehu();
        when_i_request_a_list_of_weather_stations_within_n_kms(50);
        then_the_number_of_returned_stations_is(6);
        then_all_the_stations_are_within_n_kms(50);
    }

    @Test
    public void stationDataForChateauEWS() throws HttpException, InvalidResponseException {
        given_the_client_is_logged_in();
        given_station_data_params_for_chateau_ews();
        when_i_request_station_data();
        then_the_station_name_is("Mt Ruapehu, Chateau Ews");
    }

    // Given, when, then methods

    private void given_the_client_is_logged_in() throws HttpException {
        final LoginParams loginParams = new LoginParams("jonathan.aotearoa", "WTBPHTNuMVik3rTo");
        client.login(loginParams);
    }

    private void given_coordinate_params_for_mt_ruapehu() {
        coordinatesParams = new CoordinatesParams("-39.275888", "175.558106");
    }

    private void when_i_request_a_list_of_weather_stations_within_n_kms(final int kms) throws InvalidResponseException, HttpException {
        stationsByLocation = client.getStations(dataTypeParams, ImmutableList.of(coordinatesParams), kms);
    }

    private void then_the_number_of_returned_stations_is(final int numberOfStations) {
        assertNotNull(stationsByLocation);
        final List<Station> stations = stationsByLocation.get(coordinatesParams);
        assertNotNull(stations);
        assertEquals(numberOfStations, stations.size());
    }

    private void given_station_data_params_for_chateau_ews() {
        stationDataParams = new StationDataParams(18464, dataTypeParams, localDate);
    }

    private void when_i_request_station_data() throws InvalidResponseException, HttpException {
        stationDataResponse = client.getStationData(stationDataParams);
    }

    private void then_the_station_name_is(final String stationName) {
        assertNotNull(stationDataResponse);
        assertEquals(stationName, stationDataResponse.getStationName());
    }

    private void then_all_the_stations_are_within_n_kms(final int kms) {
        assertNotNull(stationsByLocation);
        List<Station> stations = stationsByLocation.get(coordinatesParams);
        assertNotNull(stations);
        final BigDecimal maxDistance = BigDecimal.valueOf(kms);
        for (final Station station : stations) {
            final BigDecimal distance = station.getDistance();
            assertTrue(distance.compareTo(maxDistance) <= 0);
        }
    }
}