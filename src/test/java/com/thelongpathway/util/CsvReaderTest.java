package com.thelongpathway.util;

import com.google.common.collect.ImmutableList;
import com.thelongpathway.niwa.cliflo.ResourceUtils;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CsvReaderTest {

    private CsvReader csvReader;
    private String csvText;
    private List<Map<String, String>> csvValues;

    @Test
    public void validCsv() throws InvalidCsvException {
        given_a_csv_reader_with_the_delimiter(',');
        given_the_csv("ValidCSV.txt");
        when_i_read_the_csv();
        then_the_csv_values_are_as_expected();
    }

    @Test(expected = InvalidCsvException.class)
    public void missingValueCsv() throws InvalidCsvException {
        given_a_csv_reader_with_the_delimiter(',');
        given_the_csv("MissingValueCSV.txt");
        when_i_read_the_csv();
    }

    @Test(expected = InvalidCsvException.class)
    public void noValuesCsv() throws InvalidCsvException {
        given_a_csv_reader_with_the_delimiter(',');
        given_the_csv("NoValuesCSV.txt");
        when_i_read_the_csv();
    }

    // Given, when, then methods

    private void given_a_csv_reader_with_the_delimiter(final char delimiter) {
        csvReader = new CsvReader(delimiter);
    }

    private void given_the_csv(final String fileName) {
        csvText = ResourceUtils.getResourceAsString(getClass().getPackage(), fileName);
    }

    private void when_i_read_the_csv() throws InvalidCsvException {
        assertNotNull("csvReader cannot be null", csvReader);
        assertNotNull("csvText cannot be null", csvText);
        csvValues = csvReader.read(csvText);
    }

    private void then_the_csv_values_are_as_expected() {
        assertNotNull("csvValues cannot be null", csvValues);
        final int nRows = 4;
        final List<String> headings = ImmutableList.of("heading1", "heading2", "heading3");
        assertEquals(nRows, csvValues.size());
        for (int i = 0; i < nRows; i++) {
            final Map<String, String> row = csvValues.get(i);
            assertEquals(headings.size(), row.size());
            for (int j = 0; j < headings.size(); j++) {
                final String heading = headings.get(j);
                final String expectedValue = (j + 1) + "-" + (i + 1);
                assertEquals(expectedValue, row.get(heading));
            }
        }
    }
}