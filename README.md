# NIWA CliFlo Client

## Overview

A Java API for accessing [NIWA's National Climate Database](http://cliflo.niwa.co.nz/).
I created this API to collect weather data for my 2013/14 [Te Araroa](http://www.teararoa.org.nz/) through-hike after NIWA declined my request to use Curl.
To use the API you'll need to register with NIWA. Once you've done that you can perform a limited, but very large, number of requests.

The data collected using this API was used to generate the following [charts](https://thelongpathway.github.io/te-araroa/weather/)

### Warning

* The documentation and Javadocs assume you already know how to manually request data from NIWA's CliFlo database.
* The underlying implementation of this API is completely at the mercy of NIWA, so may break at any time.

## Functionality

The API allows you to automate the following operations:

* Login
* Request all the NIWA weather stations within a specified radius of a specified location. Referred to as a 'stations by location' request.
* Request data for a specified location on a specified day. Referred to as a 'station data' request.

## Usage

* See ``com.thelongpathway.niwa.cliflo.CliFloClientTest`` for example requests.